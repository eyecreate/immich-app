FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=1.23.0_33

RUN curl -LSs https://github.com/immich-app/immich/archive/refs/tags/v${VERSION}-dev.tar.gz | tar -xz -C /app/code/ --strip-components 1 -f -

RUN sed -e "s/immich-machine-learning/localhost/" -i /app/code/server/apps/microservices/src/processors/metadata-extraction.processor.ts
RUN cd server && npm ci
RUN cd server && npm run build

# https://github.com/immich-app/immich/blob/main/web/src/api/api.ts#L44 we can't change the /etc/hosts to match immich-server so use localhost
RUN sed -e "s/immich-server/localhost/" -i /app/code/web/src/api/api.ts
RUN cd web && npm ci
RUN cd web && npm run build

RUN cd machine-learning && npm ci
RUN cd machine-learning && npm rebuild @tensorflow/tfjs-node --build-from-source && npm run build

# supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

RUN ln -s /app/data/upload /app/code/upload

COPY start.sh nginx.conf /app/pkg/

CMD [ "/app/pkg/start.sh" ]
