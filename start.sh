#!/bin/bash

echo "=> App startup"

echo "=> Ensure directories"
mkdir -p /app/data/upload
chown -R cloudron:cloudron /app/data

echo "=> Setup environment"
export NODE_ENV="production"
export DB_HOSTNAME="${CLOUDRON_POSTGRESQL_HOST}"
export DB_PORT="${CLOUDRON_POSTGRESQL_PORT}"
export DB_USERNAME="${CLOUDRON_POSTGRESQL_USERNAME}"
export DB_PASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"
export DB_DATABASE_NAME="${CLOUDRON_POSTGRESQL_DATABASE}"
export ENABLE_MAPBOX="false"
export MAPBOX_KEY=""
export REDIS_HOSTNAME="${CLOUDRON_REDIS_HOST}"
export REDIS_PORT="${CLOUDRON_REDIS_PORT}"
export REDIS_PASSWORD="${CLOUDRON_REDIS_PASSWORD}"
# Populate a secret value for `JWT_SECRET`, you can use this command: `openssl rand -base64 128`
export JWT_SECRET="somesecret"

echo "=> Start supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i immich
